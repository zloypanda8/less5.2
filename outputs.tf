output "instance_hostname" {
  value       = "${google_compute_instance.vm_instance.*.name}"
  description = "Hostname instance"
}


output "project_id" {
  value       = "${google_compute_instance.vm_instance.0.project}" 
  description = "Project id"
}
