#less5.0
Описание и данные виртулки описаны в .tf - файлах.

Обязательно
!!! terraform init -> terraform plan -> terraform apply !!!

##update for less5.1

0. SSH через root не осилил :(
1. Переменные добавил в отдельный файлик.
2. Для пользователя kabanchik создал пару ключей. Публичный ключ положил в gcp/metadata/ssh-keys, путь до второго указаывал в переменных.
3. В метадате терраформа дал права кабанчику на sudo.
4. local-exec и remote-exec сделал по видео

##update for less5.2
1. Теперь есть две настройки фаервола
2. В output теперь получаем хостнеймы и айди проекта:
```
root@instance-3:/home/rsa-key-20211004/less5.2# terraform output
instance_hostname = [
  "node1",
  "node2",
  "node3",
]
project_id = "essential-truth-328008"
```


