resource "google_compute_instance" "vm_instance" {
  count     = "${var.node_count}"
  name      = "node${count.index+1}"
  machine_type = "e2-small"
  metadata = {
           startup-script = <<SCRIPT
           echo "kabanchik ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers.d/kabanchik
         SCRIPT
  }

  connection {
    type = "ssh"
    user = "kabanchik"
    private_key = "${file(var.private_key_path)}"
    timeout = "2m"
    host = "${self.network_interface.0.access_config.0.nat_ip}"
  }

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size  = 20
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> host.list"  
  }
 
  provisioner "remote-exec" {
    inline = [
      "sudo apt update -y",
      "sudo apt install nginx -y",
      "echo Juneway  ${self.network_interface.0.access_config.0.nat_ip} ${self.name} | sudo tee /var/www/html/index.html",
      "sudo service nginx restart"
    ]
  }
}


